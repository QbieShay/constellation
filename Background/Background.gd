extends CanvasLayer

export(int) var star_amount
export(PackedScene) var star_scene
export(float) var star_min_size = 10.0
export(float) var star_max_size = 32.0

func _ready():
	for i in range(star_amount):
		var star: ColorRect = star_scene.instance()
		var min_size = rand_range( star_min_size, star_max_size)
		star.rect_min_size =  Vector2(min_size, min_size)
		star.rect_size = star.rect_min_size
		star.rect_pivot_offset = Vector2(min_size/2.0, min_size/2.0)
		var background_size = $Background.get_rect().size
		$Background.add_child(star)
		star.rect_position = Vector2(
			rand_range(0.0, background_size.x),
			rand_range(0.0, background_size.y)
			)
		star.rect_rotation = 45.0
