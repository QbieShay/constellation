extends Control
export var text:String setget setText, getText
export var color:Color setget setColor, getColor

func setText(text: String):
	$HBoxContainer/RichTextLabel.text = text

func getText() -> String:
	return $HBoxContainer/RichTextLabel.text
	
func setColor(color: Color):
	pass

func getColor() -> Color:
	return Color()