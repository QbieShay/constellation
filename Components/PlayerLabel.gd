extends Control
export var playerName:String setget setPlayerName, getPlayerName
export var playerPoints:String setget setPlayerPoints, getPlayerPoints
export var iconColor:Color setget setIconColor, getIconColor
onready var playerNameLabel = $VBoxContainer/MarginContainer/PlayerNameLabel
onready var pointsLabel = $VBoxContainer/HBoxContainer/MarginContainer/PointsLabel
onready var IconTexture = $VBoxContainer/HBoxContainer/MarginContainer2/IconTexture

func setPlayerName(text: String):
	playerNameLabel.text = text

func getPlayerName() -> String:
	return playerNameLabel.text

func setPlayerPoints(text: String):
	pointsLabel.text = text

func getPlayerPoints() -> String:
	return pointsLabel.text

func setIconColor(color: Color):
	pass

func getIconColor() -> Color:
	return Color()